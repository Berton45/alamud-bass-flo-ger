# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import FlyEvent

class FlyAction(Action2):
    EVENT = FlyEvent
    ACTION = "voler"
    RESOLVE_OBJECT = "resolve_for_operate"
