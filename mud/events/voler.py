# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class FlyEvent(Event2):
	NAME = "voler"

	def perform(self):
		if not (self.object.has_prop("flyable")):
			self.fail()
			return self.inform("voler")
		self.inform("flyable")
