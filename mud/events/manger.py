# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class MangerEvent(Event2):
	NAME = "manger"

	def perform(self):
		if not (self.object.has_prop("mangeable")):
			self.fail()
			return self.inform("manger")
		self.inform("mangeable")
		self.object.move_to(None)
